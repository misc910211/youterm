import argparse
import subprocess

from ytmusicapi import YTMusic


parser = argparse.ArgumentParser(
    prog='ytmusic',
    description='Control Youtube Music from the command line')

parser.add_argument('song')


args = parser.parse_args() 
ytmusic = YTMusic("oauth.json")
search_results = ytmusic.search(args.song, filter="songs")
videoID = search_results[0]['videoId']

subprocess.Popen('mpv "https://www.youtube.com/watch?v={}" --no-video'.format(videoID), shell=True,
                 stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


